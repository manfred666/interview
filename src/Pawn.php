<?php

class Pawn extends Figure {
    public function __toString() {
        return $this->isBlack ? '♟' : '♙';
    }

    // проверка допустимости хода, с учетом цвета и ситуации на доске, переопределение родительской функции 
    public function validMove($xFrom, $yFrom, $xTo, $yTo) { // x = "e", y = "2"
        // на одну клетку вперед, если клетка не занята (cellState === 1)
        if (
            // горизонталь не изменилась
            $xFrom === $xTo &&
            // ход на 1 клетку вперед (зависит от цвета)
            ($this->isBlack ? ($yFrom - 1) === ($yTo - 0) : ($yFrom + 1) === ($yTo + 0)) &&
            // клетка на которую хотим пойти не занята
            $this->desk->cellState($xTo, $yTo, $this->isBlack) === 1
        ) { return true; }
        // на одну клетку вперед и вправо если клетка занята противником (cellState === 3)
        if (
            // на одну клетку вправо
            chr($xFrom++) === chr($xTo) &&
            // ход на 1 клетку вперед (зависит от цвета)
            ($this->isBlack ? ($yFrom - 1) === ($yTo - 0) : ($yFrom + 1) === ($yTo + 0)) &&
            // клетка на которую хотим пойти занята противником
            $this->desk->cellState($xTo, $yTo, $this->isBlack) === 3
        ) { return true; }
        // на одну клетку вперед и влево если клетка занята противником (cellState === 3)
        if (
            // на одну клетку влево
            chr($xFrom--) === chr($xTo) &&
            // ход на 1 клетку вперед (зависит от цвета)
            ($this->isBlack ? ($yFrom - 1) === ($yTo - 0) : ($yFrom + 1) === ($yTo + 0)) &&
            // клетка на которую хотим пойти занята противником
            $this->desk->cellState($xTo, $yTo, $this->isBlack) === 3
        ) { return true; }
        // через одну клетку вперед, если клетка свободна и если клетка перед ней свободна если и пешка еще не ходила
        if (
            // горизонталь не изменилась
            chr($xFrom) === chr($xTo) &&
            // ходим на две клетки вперед (зависит от цвета)
            ($this->isBlack ? ($yFrom - 2) === ($yTo - 0) : ($yFrom + 2) === ($yTo + 0)) &&
            // клетка перед фигурой свободна
            $this->desk->cellState($xTo, ($this->isBlack ? $yFrom - 1 : $yFrom + 1), $this->isBlack) === 1 &&
            // клетка через клетку от фигуры свободна
            $this->desk->cellState($xTo, ($this->isBlack ? $yFrom - 2 : $yFrom + 2), $this->isBlack) === 1 &&
            // фигурой еще не ходили
            !$this->touched
        ) { return true; }

        return false;
    }
    
}
