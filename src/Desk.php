<?php

class Desk {
    private $figures = [];
    // чей ход
    private $whose_turn = false; // isBlack = false

    public function __construct() {
        $this->figures['a'][1] = new Rook(false, $this);
        $this->figures['b'][1] = new Knight(false, $this);
        $this->figures['c'][1] = new Bishop(false, $this);
        $this->figures['d'][1] = new Queen(false, $this);
        $this->figures['e'][1] = new King(false, $this);
        $this->figures['f'][1] = new Bishop(false, $this);
        $this->figures['g'][1] = new Knight(false, $this);
        $this->figures['h'][1] = new Rook(false, $this);

        $this->figures['a'][2] = new Pawn(false, $this);
        $this->figures['b'][2] = new Pawn(false, $this);
        $this->figures['c'][2] = new Pawn(false, $this);
        $this->figures['d'][2] = new Pawn(false, $this);
        $this->figures['e'][2] = new Pawn(false, $this);
        $this->figures['f'][2] = new Pawn(false, $this);
        $this->figures['g'][2] = new Pawn(false, $this);
        $this->figures['h'][2] = new Pawn(false, $this);

        $this->figures['a'][7] = new Pawn(true, $this);
        $this->figures['b'][7] = new Pawn(true, $this);
        $this->figures['c'][7] = new Pawn(true, $this);
        $this->figures['d'][7] = new Pawn(true, $this);
        $this->figures['e'][7] = new Pawn(true, $this);
        $this->figures['f'][7] = new Pawn(true, $this);
        $this->figures['g'][7] = new Pawn(true, $this);
        $this->figures['h'][7] = new Pawn(true, $this);

        $this->figures['a'][8] = new Rook(true, $this);
        $this->figures['b'][8] = new Knight(true, $this);
        $this->figures['c'][8] = new Bishop(true, $this);
        $this->figures['d'][8] = new Queen(true, $this);
        $this->figures['e'][8] = new King(true, $this);
        $this->figures['f'][8] = new Bishop(true, $this);
        $this->figures['g'][8] = new Knight(true, $this);
        $this->figures['h'][8] = new Rook(true, $this);
    }

    public function move($move) {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo   = $match[3];
        $yTo   = $match[4];

        if (isset($this->figures[$xFrom][$yFrom])) {
            // проверим последовательность хода
            if($this->whose_turn !== $this->figures[$xFrom][$yFrom]->isBlack()) throw new \Exception("Move order error");
            // проверка валидности хода
            if (!$this->figures[$xFrom][$yFrom]->validMove($xFrom, $yFrom, $xTo, $yTo))  throw new \Exception("Invalid move");

            // сделаем ход
            $this->figures[$xTo][$yTo] = $this->figures[$xFrom][$yFrom];
            // отметим фигуру как "сделавшую ход"
            $this->figures[$xFrom][$yFrom]->touched = true;
            // передадим ход
            $this->whose_turn = !$this->whose_turn;
        } else {
        // ругаемся если пытаемся сделать ход из пустой клетки
            throw new \Exception("From cell is empty");
        }
        unset($this->figures[$xFrom][$yFrom]);
    }

    public function dump() {
        for ($y = 8; $y >= 1; $y--) {
            echo "$y ";
            for ($x = 'a'; $x <= 'h'; $x++) {
                if (isset($this->figures[$x][$y])) {
                    echo $this->figures[$x][$y];
                } else {
                    echo '-';
                }
            }
            echo "\n";
        }
        echo "  abcdefgh\n";
    }

    // состояние клетки
    //   0 - мимо доски
    //   1 - клетка свободна
    //   2 - клетка занята нашей фигурой
    //   3 - клетка занята фигурой противника
    public function cellState($x, $y, $isBlack) {
        // попали в доску
        if ($y < 9 && $y > 0 && $x >= 'a' && $x <= 'h') {
            // есть фигура
            if ($this->figures[$x][$y]) {
                // наша фигура
                if ($this->figures[$x][$y]->isBlack() === $isBlack) {
                    return 2;
                } else {
                // чужая фигура
                    return 3;    
                }
            } else {
            // нет фигуры
                return 1;    
            }
        } else {
        // мимо доски 
            return 0;
        }
    }

}
