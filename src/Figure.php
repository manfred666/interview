<?php

class Figure {
    protected $isBlack;
    protected $desk;
    // наличие ходов фигурой
    public $touched = false;

    // public function __construct($isBlack) {
    //     $this->isBlack = $isBlack;
    // }

    public function __construct($isBlack, $desk) {
        $this->isBlack = $isBlack;
        // ссылка на доску, для доступа к обстановке вокруг фигуры
        $this->desk = $desk;
    }

    /** @noinspection PhpToStringReturnInspection */
    public function __toString() {
        throw new \Exception("Not implemented");
    }

    // доступ к protected свойству
    public function isBlack() {
        return $this->isBlack;
    }

    public function validMove($xFrom, $yFrom, $xTo, $yTo) {
        return true;
    }
}
