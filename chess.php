<?php

try {
    foreach (glob('src/*.php') as $file) {
        require_once $file;
    }
    // инициализация доски, расстановка фигур
    $desk = new Desk();
    // чтение аргументов
    $args = $argv;
    // пропускаем первый аргумент - chess.php
    array_shift($args);
    // пробег по ходам
    foreach ($args as $move) {
        $desk->move($move);
    }
    // вывод доски
    $desk->dump();
} catch (\Exception $e) {
    echo $e->getMessage() . "\n";
    exit(1);
}
